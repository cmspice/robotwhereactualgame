﻿using UnityEngine;
using System.Collections.Generic;
using FarseerPhysics;
using FarseerPhysics.Factories;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using FarseerPhysics.Collision;
using FarseerPhysics.Controllers;
using FarseerPhysics.Dynamics.Contacts;
using System;
using System.Linq;



public class GraphicsGroup{
    public string Name{get;set;}
    public Vector2 Size{get;set;}
    public Vector2 PosOffset{get;set;}
    public float RotOffset{get;set;}
    public int Depth{get;set;}

    public GraphicsGroup(string aName, Vector2 aSize){
        Name = aName;
        Size = aSize;
        Depth = 100;
    }
    public GraphicsGroup(string aName, Vector2 aSize, Vector2 aPosOffset, float aRotOffset = 0, int aDepth = 100):this(aName,aSize){
        PosOffset = aPosOffset;
        RotOffset = aRotOffset;
        Depth = aDepth;
    }

}


public class RuTest : MonoBehaviour {

    public const float sArmDistance = .18f;
    public const float sLegDistance = .25f;
    public const float sLimbThickness = .07f;   

    public static Dictionary<string,GraphicsGroup> sSizes = new Dictionary<string,GraphicsGroup>()
    {
        {"Torso",new GraphicsGroup("Torso",new Vector2(.7f,1))},
        {"Head",new GraphicsGroup("Head",new Vector2(.7f,.4f))},

        {"rPalm",new GraphicsGroup("rPalm",new Vector2(sArmDistance,sLimbThickness),new Vector2(sArmDistance/2f,0),0,101)},
        {"rElbow",new GraphicsGroup("rElbow",new Vector2(sArmDistance,sLimbThickness),new Vector2(sArmDistance/2f,0),0,101)},
        {"rShoulder",new GraphicsGroup("rShoulder",new Vector2(sArmDistance,sLimbThickness),new Vector2(sArmDistance/2f,0),0,101)},

        {"rFoot",new GraphicsGroup("rFoot",new Vector2(sLegDistance,sLimbThickness),new Vector2(sLegDistance/2f,0),0,101)},
        {"rKnee",new GraphicsGroup("rKnee",new Vector2(sLegDistance,sLimbThickness),new Vector2(sLegDistance/2f,0),0,101)},
        {"rHip",new GraphicsGroup("rHip",new Vector2(sArmDistance,sLimbThickness),new Vector2(sLegDistance/2f,0),0,101)},

        {"lPalm",new GraphicsGroup("lPalm",new Vector2(sArmDistance,sLimbThickness),new Vector2(sArmDistance/2f,0),0,101)},
        {"lElbow",new GraphicsGroup("lElbow",new Vector2(sArmDistance,sLimbThickness),new Vector2(sArmDistance/2f,0),0,101)},
        {"lShoulder",new GraphicsGroup("lShoulder",new Vector2(sArmDistance,sLimbThickness),new Vector2(sArmDistance/2f,0),0,101)},

        {"lFoot",new GraphicsGroup("lFoot",new Vector2(sLegDistance,sLimbThickness),new Vector2(sLegDistance/2f,0),0,101)},
        {"lKnee",new GraphicsGroup("lKnee",new Vector2(sLegDistance,sLimbThickness),new Vector2(sLegDistance/2f,0),0,101)},
        {"lHip",new GraphicsGroup("lHip",new Vector2(sArmDistance,sLimbThickness),new Vector2(sLegDistance/2f,0),0,101)}

    };




    public Texture2D tempGraphic;

    Body mBodyA;
    Body mBodyB;
    List<Fixture> mBodyBCollisions = new List<Fixture>();
    Body mTorso;
    Body mHead;
    FarseerPhysics.Dynamics.Joints.DistanceJoint mABJoint;
    float mABInitDist;
    bool OnBCollisionEnter(Fixture thisFix,Fixture aFix,Contact aContact)
    {
        //TODO check fixture layer to be ground layer
        mBodyBCollisions.Add(aFix);
        return true;
    }
    void OnBCollisionExit(Fixture thisFix,Fixture aFix)
    {
        mBodyBCollisions.Remove(aFix);
    }


    RuEntity mRobo;
    RobotAnimator mAnimator;

	void Start () {

        //load the sizes from files
        UnityEngine.Object[] robots = Resources.LoadAll("");
        foreach (var e in sSizes)
        {
            var graphic = robots.First(f=>e.Key.Contains(f.name.Substring(2))) as Texture2D;
            e.Value.Size = new Vector2(graphic.width,graphic.height)/300f;
        }



        mBodyA = BodyFactory.CreateBody (FSWorldComponent.PhysicsWorld);
        mBodyA.Position = new FVector2 (0, 4f);
        mBodyA.BodyType = BodyType.Dynamic;
        mBodyA.Mass = 15;
        mBodyA.LinearDamping = .0f;
        mBodyA.FixedRotation = true;
        //var aFix = FixtureFactory.AttachRectangle (1, 1, 1, FVector2.Zero, mBodyA);
        var aFix = FixtureFactory.AttachRectangle (.3f, .3f, 1, FVector2.Zero, mBodyA);
        aFix.CollisionGroup = -1;
        
        mBodyB = BodyFactory.CreateBody (FSWorldComponent.PhysicsWorld);
        mBodyB.Position = new FVector2 (0, 3);
        mBodyB.BodyType = BodyType.Dynamic;
        mBodyB.Mass = 10;
        mBodyB.FixedRotation = true;
        var bFixture = FixtureFactory.AttachRectangle (.7f, .2f, 1, FVector2.Zero, mBodyB);
        bFixture.OnCollision += OnBCollisionEnter;
        bFixture.OnSeparation += OnBCollisionExit;
        bFixture.CollisionGroup = -1;

        mTorso = BodyFactory.CreateBody(FSWorldComponent.PhysicsWorld);
        mTorso.Position = mBodyA.Position;
        mTorso.BodyType = BodyType.Dynamic;
        mTorso.Mass = 1;
        mTorso.AngularDamping = 5f;
        var torsoFix = FixtureFactory.AttachRectangle(sSizes["Torso"].Size.x,sSizes["Torso"].Size.y, 1, FVector2.Zero, mTorso);
        torsoFix.CollisionGroup = -1;
        
        mHead = BodyFactory.CreateBody(FSWorldComponent.PhysicsWorld);
        mHead.Position = mTorso.Position + new FVector2(-.02f,.8f);
        mHead.BodyType = BodyType.Dynamic;
        mHead.Mass = .3f;
        mHead.AngularDamping = 5f;
        var headFix = FixtureFactory.AttachRectangle(sSizes["Head"].Size.x,sSizes["Head"].Size.y, .2f, FVector2.Zero, mHead);
        headFix.CollisionGroup = -1;
        
        
        Limb rArm = new Limb();
        var rShoulder = rArm.AddBody(0,mTorso.Position + new FVector2(.2f,.3f));
        var rElbow = rArm.AddBody(sSizes["rShoulder"].Size.x);
        var rPalm = rArm.AddBody(sSizes["rElbow"].Size.x);
        
        Limb lArm = new Limb();
        var lShoulder = lArm.AddBody(0,mTorso.Position + new FVector2(-.2f,.3f));
        var lElbow = lArm.AddBody(sSizes["lShoulder"].Size.x);
        var lPalm = lArm.AddBody(sSizes["rElbow"].Size.x);

        Limb rLeg = new Limb();
        var rHip = rLeg.AddBody(0, mTorso.Position + new FVector2(.25f, -.5f));
        var rKnee = rLeg.AddBody(sSizes["rHip"].Size.x);
        var rFoot = rLeg.AddBody(sSizes["rKnee"].Size.x);

        Limb lLeg = new Limb();
        var lHip = lLeg.AddBody(0, mTorso.Position + new FVector2(-.25f, -.5f));
        var lKnee = lLeg.AddBody(sSizes["lHip"].Size.x);
        var lFoot = lLeg.AddBody(sSizes["lKnee"].Size.x);
        

		//joints
		//prismatic joint fixes angle and lateral motion
		var abPrismatic = JointFactory.CreatePrismaticJoint(FSWorldComponent.PhysicsWorld,mBodyA, mBodyB, FVector2.Zero, new FVector2(0, 1));
		abPrismatic.LimitEnabled = true;
		abPrismatic.LowerLimit = -1;
		abPrismatic.UpperLimit = 1;
		//distance joint used to adjust distance
		mABJoint = JointFactory.CreateDistanceJoint(FSWorldComponent.PhysicsWorld, mBodyA, mBodyB, FVector2.Zero, FVector2.Zero);
		mABJoint.Frequency = 15;
		mABJoint.DampingRatio = .8f;
		mABInitDist = mABJoint.Length;

		var torsoARobotJoint = new FarseerPhysics.Dynamics.Joints.RobotJoint2(mBodyA, mTorso, mBodyA.Position);
		torsoARobotJoint.RotationStiffness = 3;
		torsoARobotJoint.Frequency = 10.0f;
		torsoARobotJoint.DampingRatio = .9f;
		torsoARobotJoint.UseDistanceSpring = true;
		torsoARobotJoint.EffectFirstBody = false;
		FSWorldComponent.PhysicsWorld.AddJoint(torsoARobotJoint);

		var headTorsoJoint = new FarseerPhysics.Dynamics.Joints.RobotJoint2(mTorso, mHead, mHead.Position);
		headTorsoJoint.RotationStiffness = 3;
		headTorsoJoint.Frequency = 10.0f;
		headTorsoJoint.DampingRatio = .9f;
		headTorsoJoint.UseDistanceSpring = true; 
		headTorsoJoint.EffectFirstBody = false;
		//headTorsoJoint.ReferenceAngle = 90 * Mathf.PI / 180f;
		FSWorldComponent.PhysicsWorld.AddJoint(headTorsoJoint);

		var lLegJoint = new FarseerPhysics.Dynamics.Joints.RobotJoint2(mTorso, lHip, lHip.Position);
		lLegJoint.RotationStiffness = .01f;
		lLegJoint.UseDistanceSpring = false;
		lLegJoint.EffectFirstBody = false;
		FSWorldComponent.PhysicsWorld.AddJoint(lLegJoint);
		var rLegJoint = new FarseerPhysics.Dynamics.Joints.RobotJoint2(mTorso, rHip, rHip.Position);
		rLegJoint.RotationStiffness = .01f;
		rLegJoint.UseDistanceSpring = false;
		rLegJoint.EffectFirstBody = false;
		FSWorldComponent.PhysicsWorld.AddJoint(rLegJoint);

		//var lArmJoint = JointFactory.CreateRevoluteJoint(FSWorldComponent.PhysicsWorld, mTorso, lShoulder, FVector2.Zero);
		var lArmJoint = new FarseerPhysics.Dynamics.Joints.RobotJoint2(mTorso,lShoulder,lShoulder.Position);
		lArmJoint.RotationStiffness = .01f;
		lArmJoint.UseDistanceSpring = false;
		lArmJoint.EffectFirstBody = false;
		FSWorldComponent.PhysicsWorld.AddJoint(lArmJoint);
		//var rArmJoint = JointFactory.CreateRevoluteJoint(FSWorldComponent.PhysicsWorld, mTorso, rShoulder, FVector2.Zero);
		var rArmJoint = new FarseerPhysics.Dynamics.Joints.RobotJoint2(mTorso,rShoulder,rShoulder.Position);
		rArmJoint.RotationStiffness = .01f;
		rArmJoint.UseDistanceSpring = false;
		rArmJoint.EffectFirstBody = false;
		FSWorldComponent.PhysicsWorld.AddJoint(rArmJoint);

        
        mRobo = new RuEntity();
        System.IO.FileStream stream = new System.IO.FileStream("hi.txt", System.IO.FileMode.Open);
        mRobo = (RuEntity)(new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter().Deserialize(stream));
        stream.Close();

        //now connect it to RuAnim
        mRobo.add_body(mBodyA, "bodyA");
        mRobo.add_body(mBodyB, "bodyB");
        mRobo.add_body(mTorso, "Torso");
        mRobo.add_body(mHead, "Head");
        mRobo.add_body(rShoulder, "rShoulder");
        mRobo.add_body(rElbow, "rElbow");
        mRobo.add_body(rPalm, "rPalm");
        mRobo.add_body(lShoulder, "lShoulder");
        mRobo.add_body(lElbow, "lElbow");
        mRobo.add_body(lPalm, "lPalm");
        mRobo.add_body(rHip, "rHip");
        mRobo.add_body(rKnee, "rKnee");
        mRobo.add_body(rFoot, "rFoot");
        mRobo.add_body(lHip, "lHip");
        mRobo.add_body(lKnee, "lKnee");
        mRobo.add_body(lFoot, "lFoot");

        mRobo.add_joint(abPrismatic, "ABPrismatic");
        mRobo.add_joint(mABJoint, "ABDistance");
        mRobo.add_joint(torsoARobotJoint, "torsoARobot");
        mRobo.add_joint(headTorsoJoint, "headTorsoRobot");
		//mRobo.add_hard_joint(mBodyA,mTorso,Vector2.zero,"torsoARobot");
		//mRobo.add_hard_joint(mTorso,mHead,(mHead.Position-mTorso.Position).to_Vector2(),"headTorsoRobot");


        mRobo.add_joint(rArmJoint, "rArmTorsoRobot");
        mRobo.add_joint(lArmJoint, "lArmTorsoRobot");
        mRobo.add_joint(rArm.mJoints[1], "rArm1");
        mRobo.add_joint(lArm.mJoints[1], "lArm1");
        mRobo.add_joint(rArm.mJoints[0], "rArm0");
        mRobo.add_joint(lArm.mJoints[0], "lArm0");

        mRobo.add_joint(rLegJoint, "rLegTorsoRobot");
        mRobo.add_joint(lLegJoint, "lLegTorsoRobot");
        mRobo.add_joint(rLeg.mJoints[1], "rLeg1");
        mRobo.add_joint(lLeg.mJoints[1], "lLeg1");
        mRobo.add_joint(rLeg.mJoints[0], "rLeg0");
        mRobo.add_joint(lLeg.mJoints[0], "lLeg0");




        //add the graphics
        foreach (var e in sSizes)
        {
            var graphic = robots.First(f=>e.Key.Contains(f.name.Substring(2)));
            mRobo.add_body_graphic(e.Key, graphic as Texture2D, e.Value.PosOffset, e.Value.RotOffset, e.Value.Size, e.Value.Depth);
        }

        //update mRobo with the current values 
        mRobo.update_joints_with_FS_values();
        


        //animations
        mAnimator = new RobotAnimator(mRobo);

	}
	


    void OnGUI()
    {
        (new RuKeyboardAnimation()).OnGUI(mRobo);
    }

    int fileCounter = 0;
	void FixedUpdate () {

        //update the animations
        (new RuKeyboardAnimation()).Operate(mRobo);

        //mAnimator.Update();

        //update the graphics
        mRobo.update();





        //use this to record animations
        if (Input.GetKeyDown(KeyCode.Space))
        {
            System.IO.FileStream stream = new System.IO.FileStream("totalrobot.txt", System.IO.FileMode.Create);
            new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter().Serialize(stream, mRobo);
            stream.Close();

            stream = new System.IO.FileStream("frame"+fileCounter+".txt", System.IO.FileMode.Create);
            new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter().Serialize(stream, new RuKeyFrame(mRobo.mRuJointMap));
            stream.Close();
            fileCounter++;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            if(Mathf.Abs(mBodyA.LinearVelocity.X) < .1f)
                mBodyA.ApplyLinearImpulse(new FVector2(.1f,0)*mBodyA.Mass);
            mBodyA.ApplyForce(new FVector2(10,0));
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if(Mathf.Abs(mBodyA.LinearVelocity.X) < .1f)
                mBodyA.ApplyLinearImpulse(new FVector2(-.1f,0)*mBodyA.Mass);
            mBodyA.ApplyForce(new FVector2(-10,0));
        }


        float desiredLength = mABJoint.Length;
        if (Input.GetKey(KeyCode.DownArrow))
            desiredLength = Mathf.MoveTowards(mABJoint.Length,.85f,.05f);
        else
            desiredLength = Mathf.MoveTowards(mABJoint.Length,mABInitDist,.05f);
        
        if(desiredLength != mABJoint.Length)
            mBodyA.ApplyForce(new FVector2(0,-40));
        mABJoint.Length = desiredLength;
        
        //this is kind ofbad because it wont let you jump if you are reall yclose to the ground 
        if(mBodyBCollisions.Count != 0 && Input.GetKeyDown(KeyCode.UpArrow))
            mBodyA.ApplyForce(new FVector2(0,40));

	
	}
}
