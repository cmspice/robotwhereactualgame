﻿using UnityEngine;
using System.Collections.Generic;
using FarseerPhysics;
using FarseerPhysics.Factories;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using FarseerPhysics.Collision;
using FarseerPhysics.Controllers;
using FarseerPhysics.Dynamics.Contacts;
using System;
using System.Linq;


public class Limb 
{
    //chain of revolute joints...
    //accessor for top joint
    //accessor for bottom joint
    string Name {get; set;} //Needed for recording
    public List<Body> mChain = new List<Body>();
    //public List<FarseerPhysics.Dynamics.Joints.RevoluteJoint> mJoints = new List<FarseerPhysics.Dynamics.Joints.RevoluteJoint>();
    public List<FarseerPhysics.Dynamics.Joints.FarseerJoint> mJoints = new List<FarseerPhysics.Dynamics.Joints.FarseerJoint>();

    //TODO do this with progressively lighter mass
    public Body AddBody(float aDistance, FVector2? aPos = null){
        FVector2 pos = aPos.HasValue ? aPos.Value : FVector2.Zero;
        if (mChain.Count > 0)
            pos = mChain [mChain.Count - 1].Position + aDistance * new FVector2(1, 0);
        Debug.Log(pos);
        Body r = BodyFactory.CreateBody(FSWorldComponent.PhysicsWorld, pos);
        r.Mass = 0.1f;
        r.AngularDamping = 100f;
        r.LinearDamping = 10;
        r.BodyType = BodyType.Dynamic;
        var fix = FixtureFactory.AttachCircle(.05f, 1, r,FVector2.Zero);
        fix.CollisionGroup = -1;
        mChain.Add(r);
        int ind = mChain.Count;

        if (ind > 1)
        {
            FarseerPhysics.Dynamics.Joints.FarseerJoint joint;
            if(ind == 2)
            //if(true)
            //if(false)
            {
                //var wjoint = JointFactory.CreateRevoluteJoint(FSWorldComponent.PhysicsWorld,mChain[ind-1],mChain[ind-2],FVector2.Zero);
                var wjoint = JointFactory.CreateWeldJoint(FSWorldComponent.PhysicsWorld,mChain[ind-2],mChain[ind-1],mChain[ind-2].Position);
                joint = wjoint;
            }
            else
            {
                var rjoint = new FarseerPhysics.Dynamics.Joints.RobotJoint2(mChain[ind-2],mChain[ind-1],mChain[ind-2].Position);
                rjoint.RotationStiffness = .03f;
                rjoint.UseDistanceSpring = true;
                rjoint.EffectFirstBody = false; //I don't understand why this needs to be true
                //joint.ReferenceAngle = Mathf.PI/2f;
                joint = rjoint;
            }
            

            FSWorldComponent.PhysicsWorld.AddJoint(joint);
            mJoints.Add(joint);
        }

        return r;
    }

    public void Write()
    {
        /*
            System.IO.Stream stream = System.IO.File.Open(aFile, System.IO.FileMode.Create);
            System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(typeof(Pose));
            xs.Serialize(stream, p);
            stream.Close();
            Debug.Log("write file " + aFile);
        */
    }
}

//TODO DELETE
public class OldRobot : MonoBehaviour
{



	Body mBodyA;
	Body mBodyB;
    List<Fixture> mBodyBCollisions = new List<Fixture>();

    Body mTorso;
    Body mHead;

    FarseerPhysics.Dynamics.Joints.DistanceJoint mABJoint;
    float mABInitDist;

    bool OnBCollisionEnter(Fixture thisFix,Fixture aFix,Contact aContact)
    {
        //TODO check fixture layer to be ground layer
        mBodyBCollisions.Add(aFix);
        return true;
    }
    void OnBCollisionExit(Fixture thisFix,Fixture aFix)
    {
        mBodyBCollisions.Remove(aFix);

    }

	public void Start()
      	{

		mBodyA = BodyFactory.CreateBody (FSWorldComponent.PhysicsWorld);
		mBodyA.Position = new FVector2 (0, 4f);
		mBodyA.BodyType = BodyType.Dynamic;
		mBodyA.Mass = 15;
        mBodyA.LinearDamping = .0f;
        mBodyA.FixedRotation = true;
		var aFix = FixtureFactory.AttachRectangle (1, 1, 1, FVector2.Zero, mBodyA);
        aFix.CollisionGroup = -1;

		mBodyB = BodyFactory.CreateBody (FSWorldComponent.PhysicsWorld);
		mBodyB.Position = new FVector2 (0, 3);
		mBodyB.BodyType = BodyType.Dynamic;
		mBodyB.Mass = 10;
        mBodyB.FixedRotation = true;
		var bFixture = FixtureFactory.AttachRectangle (.7f, .2f, 1, FVector2.Zero, mBodyB);
        bFixture.OnCollision += OnBCollisionEnter;
        bFixture.OnSeparation += OnBCollisionExit;
        bFixture.CollisionGroup = -1;


        //prismatic joint fixes angle and lateral motion
        var abPrismatic = JointFactory.CreatePrismaticJoint(FSWorldComponent.PhysicsWorld,mBodyA, mBodyB, FVector2.Zero, new FVector2(0, 1));
        abPrismatic.LimitEnabled = true;
        abPrismatic.LowerLimit = 0;
        abPrismatic.UpperLimit = 1;


        //distance joint used to adjust distance
        mABJoint = JointFactory.CreateDistanceJoint(FSWorldComponent.PhysicsWorld, mBodyA, mBodyB, FVector2.Zero, FVector2.Zero);
        mABJoint.Frequency = 15;
        mABJoint.DampingRatio = .8f;
        mABInitDist = mABJoint.Length;


        mTorso = BodyFactory.CreateBody(FSWorldComponent.PhysicsWorld);
        mTorso.Position = mBodyA.Position;
        mTorso.BodyType = BodyType.Dynamic;
        mTorso.Mass = 1;
        mTorso.AngularDamping = 5f;
        var torsoFix = FixtureFactory.AttachRectangle(.7f, 1, 1, FVector2.Zero, mTorso);
        torsoFix.CollisionGroup = -1;
        var torsoARobotJoint = new FarseerPhysics.Dynamics.Joints.RobotJoint2(mBodyA, mTorso, mBodyA.Position);
        torsoARobotJoint.RotationStiffness = 3;
        torsoARobotJoint.Frequency = 10.0f;
        torsoARobotJoint.DampingRatio = .9f;
        torsoARobotJoint.UseDistanceSpring = true;
        torsoARobotJoint.EffectFirstBody = false;
        FSWorldComponent.PhysicsWorld.AddJoint(torsoARobotJoint);

        mHead = BodyFactory.CreateBody(FSWorldComponent.PhysicsWorld);
        mHead.Position = mTorso.Position + new FVector2(0,.75f);
        mHead.BodyType = BodyType.Dynamic;
        mHead.Mass = .3f;
        mHead.AngularDamping = 5f;
        var headFix = FixtureFactory.AttachRectangle(.7f, .4f, .2f, FVector2.Zero, mHead);
        headFix.CollisionGroup = -1;
        var headTorsoJoint = new FarseerPhysics.Dynamics.Joints.RobotJoint2(mTorso, mHead, mHead.Position);
        headTorsoJoint.RotationStiffness = 3;
        headTorsoJoint.Frequency = 10.0f;
        headTorsoJoint.DampingRatio = .9f;
        headTorsoJoint.UseDistanceSpring = true; 
        headTorsoJoint.EffectFirstBody = false;
        FSWorldComponent.PhysicsWorld.AddJoint(headTorsoJoint); 

        Limb rArm = new Limb();
        var rShoulder = rArm.AddBody(0,mTorso.Position + new FVector2(.3f,.3f));
        var rElbow = rArm.AddBody(.3f);
        var rPalm = rArm.AddBody(.3f);
        var rArmJoint = JointFactory.CreateRevoluteJoint(FSWorldComponent.PhysicsWorld, mTorso, rShoulder, FVector2.Zero);
       
        Limb lArm = new Limb();
        var lShoulder = lArm.AddBody(0,mTorso.Position + new FVector2(-.3f,.3f));
        var lElbow = lArm.AddBody(.3f);
        var lPalm = lArm.AddBody(.3f);
        var lArmJoint = JointFactory.CreateRevoluteJoint(FSWorldComponent.PhysicsWorld, mTorso, lShoulder, FVector2.Zero);

        //var lArmJoint = new FarseerPhysics.Dynamics.Joints.RobotJoint2(mTorso,lShoulder,lShoulder.Position);
        //lArmJoint.RotationStiffness = .01f;
        //lArmJoint.UseDistanceSpring = false;
        //lArmJoint.EffectFirstBody = false;
        //lArmJoint.ReferenceAngle = Mathf.PI/2f;
        FSWorldComponent.PhysicsWorld.AddJoint(lArmJoint);


        //TODO fit gameobjects..
        //resources
        var art = Resources.LoadAll("ART/robot").Select(e => e as Texture2D);

	}




    public void fit_gameobject_to_body(GameObject aGo, Body aBody, float angleOffset = 0)
    {
        aGo.transform.position = new Vector3(aBody.Position.X, aBody.Position.Y, 0);
        aGo.transform.rotation = MathExtensions.from_flat_rotation(angleOffset + aBody.Rotation);
    }

	public void Update()
	{
        if (Input.GetKey(KeyCode.RightArrow))
        {
            if(Mathf.Abs(mBodyA.LinearVelocity.X) < .1f)
                mBodyA.ApplyLinearImpulse(new FVector2(.1f,0)*mBodyA.Mass);
            mBodyA.ApplyForce(new FVector2(10,0));
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if(Mathf.Abs(mBodyA.LinearVelocity.X) < .1f)
                mBodyA.ApplyLinearImpulse(new FVector2(-.1f,0)*mBodyA.Mass);
            mBodyA.ApplyForce(new FVector2(-10,0));
        }

        float desiredLength = mABJoint.Length;
        if (Input.GetKey(KeyCode.DownArrow))
            desiredLength = Mathf.MoveTowards(mABJoint.Length,.85f,.05f);
        else
            desiredLength = Mathf.MoveTowards(mABJoint.Length,mABInitDist,.05f);

        if(desiredLength != mABJoint.Length)
            mBodyA.ApplyForce(new FVector2(0,-40));
        mABJoint.Length = desiredLength;


        //this is kind ofbad because it wont let you jump if you are reall yclose to the ground 
        if(mBodyBCollisions.Count != 0 && Input.GetKeyDown(KeyCode.UpArrow))
            mBodyA.ApplyForce(new FVector2(0,400));
	}
}
