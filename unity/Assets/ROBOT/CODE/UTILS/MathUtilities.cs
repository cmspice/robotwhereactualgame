﻿using UnityEngine;
using System.Collections;

public class MathUtilities {
    public static float interpolate_angle_degrees(float first, float second, float lambda)
    {
        float shortest_angle=((((second - first) % 360f) + 540f) % 360f) - 180f;
        return shortest_angle * lambda;
    }
}
