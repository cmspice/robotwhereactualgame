using UnityEngine;
using System.Collections;
public struct TRS {
	public Vector3 t;
	public Quaternion r;
	public Vector3 s;

	public static TRS identity
	{
		get{
			TRS ret = new TRS();
			ret.t = Vector3.zero;
			ret.r = Quaternion.identity;
			ret.s = Vector3.one;
			return ret;
		}
	}
	/*public TRS()
	{
		t = Vector3.zero;
		r = Quaternion.identity;
		s = Vector3.one;
	}*/
	public TRS(Vector3 ap, Quaternion ar){t = ap; r = ar; s = Vector3.one;}
	//kind of bad ebcause there is no absolute scale..
	//TODO write an absolute scale function
	public TRS(Transform at) { t = at.position; r = at.rotation; s = at.localScale;}
	public TRS(Matrix4x4 aMat)
	{
		throw new UnityException("Peter was too lazy to implement");	
	}

	public TRS inverse()
	{
		return TRS.identity/this;
	}

	public Matrix4x4 matrix()
	{
		return Matrix4x4.TRS(t,r,s);
	}

	//TODO struct nonsense??? What dose this function even do
	//CCW in right handed coordinates..
	/*public void set_flat_rotation(float aAngle)
	{
		r = Quaternion.AngleAxis(aAngle,Vector3.forward);
	}*/

	public static TRS interpolate_linear(TRS A, TRS B, float t)
	{
		TRS r = new TRS();
		r.t = A.t*(1-t) + B.t*t;
		r.s = A.s*(1-t) + B.s*t;
		r.r = Quaternion.Slerp(A.r,B.r,t);
		return r;
	}

	//computes spatial position of A relative to B
	public static TRS operator /(TRS A,TRS B)
	{
		//return new SpatialPosition(Matrix4x4.Inverse(B.matrix)*A.matrix);
		TRS r = new TRS();
		r.t = Quaternion.Inverse(B.r)*((A.t-B.t).component_multiply(B.s));
		r.s = A.s.component_divide(B.s);
		r.r = A.r * Quaternion.Inverse(B.r);
		return r;
	}

	public static TRS operator *(TRS A,TRS B)
	{
		TRS r = new TRS();
		r.t = (B.r*(A.t)).component_divide(B.s)+B.t;
		r.s = A.s.component_multiply(B.s);
		r.r = A.r * B.r;
		return r;
	}


		
}
