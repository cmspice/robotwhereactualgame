﻿using UnityEngine;
using System.Collections;

public static class FSExtensions
{
    public static Microsoft.Xna.Framework.FVector2 to_FVector2(this Vector2 aVec)
    {
        return new Microsoft.Xna.Framework.FVector2(aVec.x, aVec.y);
    }
    public static Vector2 to_Vector2(this Microsoft.Xna.Framework.FVector2 aVec)
    {
        return new Vector2(aVec.X, aVec.Y);
    }
}
