﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;


public class RuKeyframeAnimationConstructorBehaviour : MonoBehaviour{
    public string[] filenames;
    public float[] times;


    public void Start()
    {
        if (filenames.Length > 0)
        {
            RuKeyFrameAnimation anim = new RuKeyFrameAnimation();
            for (int i = 0; i < filenames.Length; i++)
            {
                System.IO.FileStream stream = new System.IO.FileStream(filenames [i] + ".txt", System.IO.FileMode.Open);
                var frame = (RuKeyFrame)(new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter().Deserialize(stream));
                stream.Close();
                anim.AddKeyframe(frame, times [i]);
            }

            var animStream = new System.IO.FileStream("animation.txt", System.IO.FileMode.Create);
            new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter().Serialize(animStream, anim);
            animStream.Close();
        }
    }
}