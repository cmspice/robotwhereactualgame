﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
public class RobotAnimator
{
    public class AnimationGroup
    {
        //in theory this should/could be IRuAnimation or something like that.
        public RuKeyFrameAnimation Animation{get;private set;}
        public string Name{get;private set;}
        //other stuff

        public AnimationGroup(RuKeyFrameAnimation aAnim, string aName)
        {
            Name = aName;
            Animation = aAnim;
        }
    }



    List<AnimationGroup> mAnimations = new List<AnimationGroup>();
    AnimationGroup mActive = null;
    RuEntity mEntity;

    public RobotAnimator(RuEntity aEntity)
    {
        mEntity = aEntity;
        //TODO read animations
        //set mACtive

        //temp stuff
        System.IO.FileStream stream = new System.IO.FileStream("animation.txt", System.IO.FileMode.Open);
        var frame = (RuKeyFrameAnimation)(new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter().Deserialize(stream));
        stream.Close();
        mAnimations.Add(new AnimationGroup(frame, "TEMP"));
        mActive = mAnimations.First();
        mActive.Animation.Mode = RuKeyFrameAnimation.AnimationMode.LOOP | RuKeyFrameAnimation.AnimationMode.PINGPONG;
        mActive.Animation.TimeScaling = .2f;
    }

    public void Update()
    {
        if (mActive != null)
        {
            mActive.Animation.Update(Time.deltaTime);
            mActive.Animation.Operate(mEntity);
        }
    }

    public void SetNewState(string aState)
    {
        mActive = mAnimations.First(e=>e.Name == aState);
    }

    public string GetState()
    {
        if (mActive == null)
            return "";
        return mActive.Name;
    }


}
