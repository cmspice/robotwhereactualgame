﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;




[Serializable]
public class RuKeyFrameAnimation
{
    [Serializable]
    public class KeyFrameGroup
    {
        public RuKeyFrame Frame { get; private set; }
        public float Time { get; set; } //absolute time
        public string Name { get; private set; } //don't really  need this
        
        public KeyFrameGroup(RuKeyFrame aFrame,float aTime, string aName = "")
        {
            Frame = aFrame;
            Time = aTime;
            Name = aName;
        }
    }
    
    //& these together
    [Serializable]
    public enum AnimationMode{
        NONE = 0,
        LOOP = 1,
        REVERSE = 2,
        PINGPONG = 4 //note in this case, a full animation loop is twice as long 
    }
    
    RuKeyFrame mLast,mTarget; //TODO DELETE mTarget

    List<KeyFrameGroup> mKeyFrameList = new List<KeyFrameGroup>();
    AnimationMode mMode = AnimationMode.NONE;
    public AnimationMode Mode{
        get{
            return mMode;
        }
        set{
            mMode = value;
            mTimer.Looping = (int)(mMode & AnimationMode.LOOP) != 0;
        }
    }
    public float TimeScaling{get;set;}
    QuTimer mTimer = new QuTimer(0, 0);
    
    
    
    public RuKeyFrameAnimation()
    {
        TimeScaling = 1f;
    }

    public void AddKeyframe(RuKeyFrame aFrame, float aTime, string aName = "")
    {
        mKeyFrameList.Add(new KeyFrameGroup(aFrame,aTime,""));
        mTimer = new QuTimer(0, aTime);
    }

    public void Update(float aTime)
    {
        mTimer.update(aTime*TimeScaling);
    }

    public void Operate(RuEntity aEntity)
    {

        int index = ((int)Time.time/3) % mKeyFrameList.Count;


        

        foreach (var e in mKeyFrameList[index].Frame.mJointMap)
        {
            RuJoint nj = e.Value;
            aEntity.get_joint(e.Key).Anchor = nj.Anchor;                                                         
            aEntity.get_joint(e.Key).Rotation = nj.Rotation;
        }


        /*
        float totalTime = mKeyFrameList.Last().Time;
        for (int i = 1; i < mKeyFrameList.Count; i++)
        {
            float timeSinceStart = mTimer.getTimeSinceStart();
            KeyFrameGroup from;
            KeyFrameGroup to;
            AnimationMode subMode = Mode;
            float process = -1;

            if((Mode & AnimationMode.PINGPONG) != 0)
            {
                timeSinceStart *= 2;
                if(timeSinceStart/totalTime > 1)
                {
                    subMode ^= AnimationMode.REVERSE;
                    timeSinceStart -= totalTime;
                }
            }

            if((subMode & AnimationMode.REVERSE) != 0)
            {
                from = mKeyFrameList [i];
                to = mKeyFrameList [i-1];
                if ((totalTime-from.Time) < timeSinceStart && timeSinceStart <= (totalTime-to.Time))
                {
                    process = (timeSinceStart - from.Time) / (to.Time - from.Time);
                }
            } else
            {
                from = mKeyFrameList [i - 1];
                to = mKeyFrameList [i];
                if (from.Time < timeSinceStart && timeSinceStart <= to.Time)
                {
                    process = (timeSinceStart - from.Time) / (to.Time - from.Time);
                }
            }

            if(process != -1)
            {
                foreach (var e in from.Frame.mJointMap)
                {
                    RuJoint nj = RuJoint.interpolate(e.Value, to.Frame.mJointMap [e.Key], process);
                    aEntity.get_joint(e.Key).Anchor = nj.Anchor;                                                         
                    aEntity.get_joint(e.Key).Rotation = nj.Rotation;
                }
                break;
            }
        }
        */
    }
}

[Serializable]
public class RuKeyFrame
{
    public Dictionary<string, RuJoint> mJointMap; //these joints need not have valid parent/children.
    
    public RuKeyFrame(Dictionary<string, RuJoint>  aJointMap)
    {
        mJointMap = aJointMap.ToDictionary(e => e.Key, e => e.Value);
    }
}