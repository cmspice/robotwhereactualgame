﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Joints;
using Microsoft.Xna.Framework;

using System.Runtime.Serialization;

//contains a tree of RuBody's that have their parent child relation defined through RuJoint
//contains mappings from RuBody to Body and RuJoint to FarseerJoint
//RuBody contains no direct reference to RuJoint so this also contains a map of RuBody to its child joints
//I'm not allowing for a child to be jointed to multiple parents (though it can be jointed twice to the same parent) or cycles in the body graph but I think these are actually all OK.
//NOTE no two bodies can have the same name or there will be issues
[Serializable]
public class RuEntity : IDeserializationCallback 
{
    Dictionary<string,RuBody> mRuBodyMap = new Dictionary<string, RuBody>();
    public Dictionary<string,RuJoint> mRuJointMap = new Dictionary<string, RuJoint>(); //should not be public
    //TODO DELETE Dictionary<RuBody,RuJoint> mBodyJointMap = new Dictionary<RuBody, RuJoint>(); //key is child of the joint, this is for convenience, the information is already available RuJoint //TODO DELETE
   
    //hooks RuEntity into physics
    //call add_body and add_joint after deserialization to connect these
    [NonSerialized]
    Dictionary<string, FarseerJoint> mJointFSJointMap = new Dictionary<string, FarseerJoint>();
    [NonSerialized]
    Dictionary<string, Body> mBodyFSBodyMap = new Dictionary<string, Body>();
    [NonSerialized]
    Dictionary<string, GameObject> mBodyGameObjectMap = new Dictionary<string, GameObject>();
    void IDeserializationCallback.OnDeserialization(System.Object sender) 
    {
        mJointFSJointMap = new Dictionary<string, FarseerJoint>();
        mBodyFSBodyMap = new Dictionary<string, Body>();
        mBodyGameObjectMap = new Dictionary<string, GameObject>();
    }



    //basic accessors
    static T get_from_dictionary<T>(Dictionary<string, T> aDict, string aKey) where T: class
    {
        if(aDict.ContainsKey(aKey))
            return aDict [aKey];
        else return null;
    }
    public RuJoint get_joint(string aName)
    {
        return get_from_dictionary(mRuJointMap, aName);
    }
    public RuBody get_body(string aName)
    {
        if(mRuBodyMap.ContainsKey(aName))
            return mRuBodyMap [aName];
        else return null;
    }
    //note these are not guaranteed to exist even if the body/joint exists resp.
    public FarseerJoint get_FSJoint(string aName)
    {
        return get_from_dictionary(mJointFSJointMap, aName);
    }
    public Body get_FSBody(string aName)
    {
        if(mBodyFSBodyMap.ContainsKey(aName))
            return mBodyFSBodyMap [aName];
        else return null;
    }
    //tree accessors
    /*
    public RuBody get_parent(RuBody aBody)
    {
        if (mBodyJointMap.ContainsKey(aBody))
            return mBodyJointMap [aBody].ParentBody;
        return null;
    }
    public IEnumerable<RuBody> get_children(RuBody aBody)
    {
        return mBodyJointMap.Where(e => e.Value.ParentBody == aBody).Select(e => e.Key);
    }*/

    //manual constructors
    public void add_body(Body aBody, string aName)
    {
        if(!mRuBodyMap.ContainsKey(aName))
            mRuBodyMap[aName] = new RuBody(aName);
        mBodyFSBodyMap[aName] = aBody;
    }
    //NOTE only call this after both parent and child FSBody of the joint has been added with add_body
    public void add_joint(FarseerJoint aJoint, string aName)
    {
        if (!mRuJointMap.ContainsKey(aName))
        {
            var parent = mRuBodyMap[mBodyFSBodyMap.First(e => e.Value == aJoint.BodyA).Key];
            var owner = mRuBodyMap[mBodyFSBodyMap.First(e => e.Value == aJoint.BodyB).Key];
            mRuJointMap[aName] = new RuJoint(aName,parent.Name, owner.Name, aJoint.JointType);
        }
        mJointFSJointMap [aName] = aJoint;
    }

    public void add_hard_joint(Body aParent, Body aChild, Vector2 aParentAnchorPoint, string aName)
    {
        if (!mRuJointMap.ContainsKey(aName))
        {
            var parent = mRuBodyMap[mBodyFSBodyMap.First(e => e.Value == aParent).Key];
            var owner = mRuBodyMap[mBodyFSBodyMap.First(e => e.Value == aChild).Key];
            mRuJointMap [aName] = new RuJoint(aName, parent.Name, owner.Name, JointType.None);
            mRuJointMap [aName].Anchor = aParentAnchorPoint;
        }
    }

    //TODO call in correct hierarchical order
    //or add a flag to do 'attempt hierarchy'
    public void add_body_graphic(string aName, Texture2D aTexture, Vector2 aOffset, float aRotOffset, Vector2? aSize = null, int aDepth = 0)
    {
        //TODO connect transform to appropriate parent
        GameObject parent = new GameObject("Ru_"+aName);
        var newSize = aSize.Value;
        //newSize.y = newSize.x * aTexture.height / (float)aTexture.width;
        GameObject go = new ImageGameObjectUtility(aTexture, newSize).PlaneObject;
        go.transform.position = aOffset;
        go.transform.rotation = MathExtensions.from_flat_rotation(aRotOffset);//*Quaternion.AngleAxis(180,Vector3.up);
        go.transform.parent = parent.transform;
        go.renderer.material.renderQueue = aDepth;
        go.renderer.material.shader = Shader.Find("Transparent/Cutout/Diffuse");
        mBodyGameObjectMap[aName] = parent;
    }

    public void update()
    {
        //TODO need to order this so it calls from TOP DOWN otherwise there will be coordinate issues
        //could also run this routine several times and that should work too
		for (int i = 0; i < 5; i++) {
			foreach (var e in mRuJointMap) {

	            if(e.Value.FSJointType == JointType.Weld)
                {
                    e.Value.update_FSJoint((WeldJoint)mJointFSJointMap[e.Key]); //weld joint is broken
                    e.Value.update_FSJoint (mBodyFSBodyMap [e.Value.ChildBody], mBodyFSBodyMap [e.Value.ParentBody]);
                }
	            else if(e.Value.FSJointType == JointType.Robot2)
                {
                    //this wont work due to script execution order...
                    //need a way to temporarily disable joints or reposition after update??
                    e.Value.update_FSJoint((RobotJoint2)mJointFSJointMap[e.Key]);
                    e.Value.update_FSJoint (mBodyFSBodyMap [e.Value.ChildBody], mBodyFSBodyMap [e.Value.ParentBody]);
                    //Debug.Log(e.Key + " " + e.Value.ChildBody + " " + e.Value.ParentBody);
                }

	            //else if(e.Value.FSJointType == JointType.Distance)
	              //  e.Value.update_FSJoint((DistanceJoint)mJointFSJointMap[e.Key]);
	            //else if(e.Value.FSJointType == JointType.Revolute)
	              //  e.Value.update_FSJoint((RevoluteJoint)mJointFSJointMap[e.Key]);
				else if(e.Value.FSJointType == JointType.None)
				{
					e.Value.update_FSJoint (mBodyFSBodyMap [e.Value.ChildBody], mBodyFSBodyMap [e.Value.ParentBody]);
				}
			}
		}

        //update the game object
        foreach (var e in mBodyGameObjectMap.Keys)
        {
            var Graphic = mBodyGameObjectMap[e]; 
            if (Graphic != null) //should never be null 
            {
                //TODO do not set position if it's a childed object
                var aBody = mBodyFSBodyMap[e];
                //TODO should not be actual position but should determined based on parent child relation (i.e. read only the angle but not the position from the body)
                Graphic.transform.position = aBody.Position.to_Vector2().to_Vector3(0); 
                Graphic.transform.rotation = MathExtensions.from_flat_rotation(aBody.Rotation*180/Mathf.PI);
            }
        }

    }

    public void update_joints_with_FS_values()
    {
        foreach (var e in mRuJointMap)
        {
            if(e.Value.FSJointType == JointType.Weld)
                e.Value.update_Joint_with_FSJoint(mBodyFSBodyMap[e.Value.ParentBody], mBodyFSBodyMap[e.Value.ChildBody],((WeldJoint)mJointFSJointMap[e.Key]).LocalAnchorA.to_Vector2());
            else if(e.Value.FSJointType == JointType.Robot2)
                e.Value.update_Joint_with_FSJoint(mBodyFSBodyMap[e.Value.ParentBody], mBodyFSBodyMap[e.Value.ChildBody],((RobotJoint2)mJointFSJointMap[e.Key]).LocalAnchorA.to_Vector2());//,((RobotJoint2)mJointFSJointMap[e.Key]).LocalAnchorB.to_Vector2());
            else
                e.Value.update_Joint_with_FSJoint(mBodyFSBodyMap[e.Value.ParentBody], mBodyFSBodyMap[e.Value.ChildBody],null);
        }
    }
}

[Serializable]
public class RuBody
{
    public string Name{get;set;}
    public RuBody(string aName)
    {
        Name = aName;
    }
}

[Serializable]
public class RuJoint
{
    public string Name{get;set;}
    //these values are used for manual control
    float mX,mY; //for serialization purposes
    public Vector2 Anchor { get{ return new Vector2(mX, mY); } set{mX = value.x; mY = value.y;} } //desired anchor of child body in parent space, LocalAnchorA
    float mCX,mCY;
    public Vector2 ChildAnchor { get{ return new Vector2(mCX, mCY); } set{mCX = value.x; mCY = value.y;} } //desired anchor of child body in child space, LocalAnchorB

    public float Rotation { get; set; } //desired angle in radians of child body relative to parent space
    public float Stiffness { get; set; } //maybe this should be like an interpolation constant instead
    //TODO consider adding additional joint parameters

    //these should really be string of names rather then direct references
    public string ParentBody { get; private set; }
    public string ChildBody { get; private set; }

    public JointType FSJointType { get; private set; }

    public RuJoint(string aName, string aParent, string aChild, JointType aJointType)
    {
        Name = aName;
        ParentBody = aParent;
        ChildBody = aChild;
        FSJointType = aJointType;
        Stiffness = 1;
    }

    //TODO do I even need this function???
    //TODO DELETE
    public void update_Joint_with_FSJoint(RuEntity aEntity, Vector2? aAnchor = null)
    {
        var aParent = aEntity.get_FSBody(ParentBody);
        var aChild = aEntity.get_FSBody(ChildBody);
        update_Joint_with_FSJoint(aParent, aChild, aAnchor);
    }

    //TODO need a set of functions for updating each type of joint...
    public void update_Joint_with_FSJoint(Body aParent, Body aChild, Vector2? aAnchor = null, Vector2? aChildAnchor = null)
    {
        if (aAnchor != null) 
            Anchor = aAnchor.Value;

        TRS parentSpace = new TRS(new Vector3(aParent.Position.X, aParent.Position.Y, 0), MathExtensions.from_flat_rotation(aParent.Rotation*180f/Mathf.PI));
        TRS anchor = new TRS(Anchor.to_Vector3(), Quaternion.identity);
        TRS childSpace = new TRS(new Vector3(aChild.Position.X, aChild.Position.Y, 0), MathExtensions.from_flat_rotation(aChild.Rotation*180f/Mathf.PI));
        Rotation = (childSpace * (anchor*parentSpace).inverse()).r.flat_rotation();

        if (aChildAnchor != null)
        {
            //Debug.Log(aChildAnchor + " " + (anchor * parentSpace * childSpace.inverse()).t.project_to_vector2());
            ChildAnchor = aChildAnchor.Value;
        }
        else
            ChildAnchor = ((anchor * parentSpace) * childSpace.inverse()).t.project_to_vector2();

    }

    //pass in childbody, must be called from TOP DOWN since we do everything in global coordinates
    //in this case, there is no FSJoint and we manually reposition the body in absolute space
    public void update_FSJoint(Body aChild, Body aParent)
    {
		TRS parentSpace = new TRS(new Vector3(aParent.Position.X, aParent.Position.Y, 0), MathExtensions.from_flat_rotation(aParent.Rotation*180f/Mathf.PI));
        TRS anchor = new TRS(Anchor.to_Vector3(), MathExtensions.from_flat_rotation(Rotation*180f/Mathf.PI));
        TRS childAnchor = new TRS(new Vector3(ChildAnchor.x, ChildAnchor.y, 0), Quaternion.identity);
        var absolute = childAnchor.inverse()*(anchor*parentSpace); 
        var nPos = absolute.t.project_to_vector2().to_FVector2();
        aChild.SetTransformIgnoreContacts(ref nPos, absolute.r.flat_rotation()*Mathf.PI/180f);

        //TODO these should not be 0
        aChild.LinearVelocity = new FVector2(0, 0);
        aChild.AngularVelocity = 0;

        //Debug.Log(aChild.Rotation + " " + aChild.Position + " " + Rotation * 180f / Mathf.PI + " " + Anchor);
        //Debug.Log(aChild.Rotation + " " + aChild.Position + " " + absolute.r.flat_rotation()*Mathf.PI/180f + " " + absolute.t.project_to_vector2().to_FVector2());
    }

    public void update_FSJoint(RobotJoint2 aJoint)
    {
        aJoint.ReferenceAngle = Rotation;
        aJoint.LocalAnchorA = Anchor.to_FVector2();
    }

    public void update_FSJoint(WeldJoint aJoint)
    {
        aJoint.ReferenceAngle = Rotation;
        aJoint.LocalAnchorA = Anchor.to_FVector2();
    }

    public void update_FSJoint(DistanceJoint aJoint)
    {
        //for distance joint we assume joint anchor is always at 0,0 and read Anchor simply as distance
        aJoint.LocalAnchorA = new FVector2(0, 0);
        aJoint.Length = Anchor.magnitude;
        //i.e. we do not do this
        //aJoint.LocalAnchorA = Anchor.to_FVector2();
    }

    public void update_FSJoint(PrismaticJoint aJoint)
    {
        aJoint.LocalAnchorA = Anchor.to_FVector2();
        //TODO 
    }

    public void update_FSJoint(RevoluteJoint aJoint)
    {
        //TODO this is broken
        TRS parentSpace = new TRS(new Vector3(aJoint.BodyA.Position.X, aJoint.BodyA.Position.Y, 0), MathExtensions.from_flat_rotation(aJoint.BodyA.Rotation));
        TRS anchor = new TRS(new Vector3(Anchor.x, Anchor.y, 0), MathExtensions.from_flat_rotation(Rotation));
        var absolute = anchor * parentSpace;
        aJoint.LocalAnchorA = Anchor.to_FVector2();
        aJoint.BodyB.Position = absolute.t.project_to_vector2().to_FVector2();
        aJoint.BodyB.Rotation = absolute.r.flat_rotation();
    }


    //NOTE the returend joint is not compatible with RuEntity since references differ
    public static RuJoint interpolate(RuJoint A, RuJoint B, float lambda)
    {
        RuJoint r = new RuJoint(A.Name + "_interpolated", A.ParentBody, A.ChildBody, A.FSJointType);
        r.Anchor = A.Anchor * (1 - lambda) + B.Anchor * (lambda);
        //r.Rotation = A.Rotation * (1 - lambda) + B.Rotation * (lambda);
        r.Rotation = MathUtilities.interpolate_angle_degrees(A.Rotation,B.Rotation,lambda);
        return r;
    }
}
