﻿using UnityEngine;
using System.Collections;


public class RuKeyboardAnimation
{
    public static KeyCode[] sKeyOrder = {
        KeyCode.Q,KeyCode.W,
        KeyCode.E,KeyCode.R,
        KeyCode.T,KeyCode.Y,
        KeyCode.U,KeyCode.I,
        KeyCode.O,KeyCode.P,
        KeyCode.A,KeyCode.S,
        KeyCode.D,KeyCode.F,
        KeyCode.G,KeyCode.H,
        KeyCode.J,KeyCode.K,
        KeyCode.L,KeyCode.Semicolon,
        KeyCode.Z,KeyCode.X,
        KeyCode.C,KeyCode.V,
        KeyCode.B,KeyCode.N,
        KeyCode.M,KeyCode.Comma
    };
    
    public void Operate(RuEntity aEntity)
    {
        int index = 0;
        foreach (var e in aEntity.mRuJointMap)
        {
            float rot = 0;
            if(Input.GetKey(sKeyOrder[index*2]))
                rot = 4*Time.deltaTime;
            if(Input.GetKey(sKeyOrder[index*2+1]))
                rot = -4*Time.deltaTime;
            e.Value.Rotation = e.Value.Rotation + rot;
            index += 1;
            if(index >= sKeyOrder.Length/2)
                break;
        }
    }

    public void OnGUI(RuEntity aEntity)
    {
        int top = 150;
        int leftPadding = 20;
        int padding = 5;

        int labelWidth = 100;
        int sliderWidth = 200;
        int height = 15;

        GUIStyle style = new GUIStyle();

        foreach (var e in aEntity.mRuJointMap)
        {
            GUI.Label(new Rect(leftPadding,top,labelWidth,height),e.Key,style);
            e.Value.Rotation = GUI.HorizontalSlider(new Rect(leftPadding + labelWidth,top,sliderWidth,height),e.Value.Rotation,0,Mathf.PI*2);
            top += padding + height;
        }
    }
}